pub use curve25519_dalek::Scalar;
use subtle::ConstantTimeEq;

/// A short scalar of only 128 bits.
#[derive(Clone, Copy, Eq, Hash)]
pub struct ShortScalar([u8; 16]);

impl ConstantTimeEq for ShortScalar {
    fn ct_eq(&self, other: &Self) -> subtle::Choice {
        self.0.ct_eq(&other.0)
    }
}

impl PartialEq for ShortScalar {
    fn eq(&self, other: &Self) -> bool {
        self.ct_eq(other).into()
    }
}

impl From<ShortScalar> for Scalar {
    fn from(value: ShortScalar) -> Self {
        let mut bytes = [0u8; 32];
        bytes.split_at_mut(16).0.copy_from_slice(&value.0);
        Scalar::from_bytes_mod_order(bytes)
    }
}

impl ShortScalar {
    pub fn from_bytes(value: [u8; 16]) -> Self {
        ShortScalar(value)
    }

    pub fn from_32(value: [u8; 32]) -> Self {
        ShortScalar(value.split_at(16).0.try_into().unwrap())
    }

    pub fn as_bytes(&self) -> &[u8; 16] {
        &self.0
    }
}

impl core::ops::Mul<Scalar> for ShortScalar {
    type Output = Scalar;

    fn mul(self, rhs: Scalar) -> Scalar {
        Scalar::from(self) * rhs
    }
}

impl core::ops::Mul<ShortScalar> for Scalar {
    type Output = Scalar;

    fn mul(self, rhs: ShortScalar) -> Scalar {
        self * Scalar::from(rhs)
    }
}

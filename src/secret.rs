use crate::{Domain, Error, Keys, PublicKey, Signature, SignerVerifier};
use curve25519_dalek::{RistrettoPoint, Scalar};
use subtle::ConstantTimeEq;
#[cfg(feature = "zeroize")]
use zeroize::{Zeroize, ZeroizeOnDrop};

/// A r255b3 secret key to sign messages with.
///
/// This is a scalar mod the group order of Ristretto255,
/// sligtly more than 2²⁵².
#[derive(Clone, Eq, Hash)]
#[cfg_attr(feature = "zeroize", derive(Zeroize, ZeroizeOnDrop))]
pub struct SecretKey(pub(crate) Scalar);

impl core::fmt::LowerHex for SecretKey {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        for x in self.0.to_bytes() {
            write!(f, "{:02x}", x)?;
        }
        Ok(())
    }
}

impl core::fmt::Debug for SecretKey {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "SecretKey {:x}", *self)
    }
}

/// This is constant time.
impl PartialEq for SecretKey {
    fn eq(&self, other: &Self) -> bool {
        self.ct_eq(other).into()
    }
}

impl ConstantTimeEq for SecretKey {
    fn ct_eq(&self, other: &Self) -> subtle::Choice {
        self.0.ct_eq(&other.0)
    }
}

impl SecretKey {
    /// Parse a secret key from an externally provided byte array,
    /// ensuring that it is a valid key.
    ///
    /// Use [SecretKey::from_bytes] for key generation.
    pub fn parse_bytes(bits: [u8; 32]) -> Result<SecretKey, Error> {
        let oscalar: Option<Scalar> = Scalar::from_canonical_bytes(bits).into();
        oscalar.map(SecretKey).ok_or(Error::InvalidSecretKey)
    }

    /// Render the secret key into a byte array for external use.
    pub fn as_bytes(&self) -> &[u8; 32] {
        self.0.as_bytes()
    }

    /// Construct a secret key from 256 arbitrary bits,
    ///
    /// Use [SecretKey::parse_bytes] if you are reading this key from storage or anything like that.
    pub fn from_bytes(value: [u8; 32]) -> SecretKey {
        SecretKey(Scalar::from_bytes_mod_order(value))
    }

    /// Sign a message, in a particular domain, with a derived nonce.
    ///
    /// This is the primary signing method.
    ///
    /// Non-application specific signatures may use empty Domain, "".
    pub fn sign(&self, domain: Domain, msg: &[u8]) -> Signature {
        SignerVerifier::init(domain).feed(msg).sign(self)
    }

    /// Derive the public key from the secret key.
    ///
    /// (Scales the base point by the secret key.)
    pub fn derive_public(&self) -> PublicKey {
        let point = RistrettoPoint::mul_base(&self.0);
        let compressed = point.compress();
        PublicKey { compressed, point }
    }

    /// Derive a complete signing key from the secret key.
    pub fn derive_keys(&self) -> Keys {
        let pk = self.derive_public();
        let sk = SecretKey(self.0);
        Keys { pk, sk }
    }

    /// Scale by a public scalar to derive a subkey.
    ///
    /// Scaling the corresponding public key by the same scalar produces a valid key pair.
    pub fn scale(&self, by: Scalar) -> Self {
        SecretKey(self.0 * by)
    }
}

use curve25519_dalek::Scalar;
use subtle::ConstantTimeEq;

use crate::{Error, ShortScalar};

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
/// A r255b3 signature.
pub struct Signature {
    pub(crate) e: ShortScalar,
    pub(crate) s: Scalar,
}

impl core::fmt::LowerHex for Signature {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        for x in self.to_bytes() {
            write!(f, "{:02x}", x)?;
        }
        Ok(())
    }
}

impl core::fmt::Debug for Signature {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "Signature {:x}", *self)
    }
}

impl Signature {
    /// View the byte encoding of the signature.
    pub fn to_bytes(&self) -> [u8; 48] {
        let mut bits = [0u8; 48];
        let (eb, sb) = bits.split_at_mut(16);
        eb.copy_from_slice(self.e.as_bytes());
        sb.copy_from_slice(self.s.as_bytes());
        bits
    }

    /// Parse a signature from a byte array, verifying its format.
    pub fn parse_bytes(value: [u8; 48]) -> Result<Signature, Error> {
        let (eb, sb) = value.split_at(16);
        let e = ShortScalar::from_bytes(eb.try_into().unwrap());
        let s = <Option<Scalar>>::from(Scalar::from_canonical_bytes(sb.try_into().unwrap()))
            .ok_or(Error::NonCanonicalSignature)?;
        Ok(Signature { e, s })
    }
}

impl ConstantTimeEq for Signature {
    fn ct_eq(&self, other: &Self) -> subtle::Choice {
        self.e.ct_eq(&other.e) & self.s.ct_eq(&other.s)
    }
}

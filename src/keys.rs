use curve25519_dalek::Scalar;

use crate::{Domain, Error, PublicKey, SecretKey, Signature, SignerVerifier};

/// A r255b3 keypair, to sign or verify messages.
///
/// This allows slightly more efficient signing for multiple messages.
#[derive(Clone, Debug)]
pub struct Keys {
    /// The secret key.
    pub sk: SecretKey,
    /// The public key.
    pub pk: PublicKey,
}

impl Keys {
    /// Verify a message in a particular domain.
    pub fn verify(&self, domain: Domain, sig: &Signature, msg: &[u8]) -> Result<(), Error> {
        self.pk.verify(domain, sig, msg)
    }

    /// Sign a message for a particular domain, with a derived nonce.
    pub fn sign(&self, domain: Domain, msg: &[u8]) -> Signature {
        SignerVerifier::init(domain)
            .feed(msg)
            .sign_for_pubkey(&self.sk, &self.pk)
    }

    /// Scale this keypair in concert.
    pub fn scale(&self, by: Scalar) -> Self {
        Keys {
            sk: self.sk.scale(by),
            pk: self.pk.scale(by),
        }
    }
}

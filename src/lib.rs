//! The r255b3 signature scheme: deterministic Schnorr signatures with Curve25519 and Blake3.
//!
//! WARNING! This signature scheme has not been thoroughly reviewed and is not standardized.
//! This code has not been audited. Use this crate at your own risk, not others!
//!
//! The r255b3 signature scheme provides short (384 bit) signatures aiming at a roughly 128 bit security level.
//! It is designed for speed - a single pass of Blake3 and the usual Schnorr-style operations for both signing and verifying.
//! It also tries to support ease of good usage: no signature malleability! domain separation required!
//!
//! ## Generating
//!
//! To sign a message you first need a [SecretKey].
//!
//! ```
//! # fn main() {
//! use r255b3::SecretKey;
//! use rand::{RngCore,rngs::OsRng};
//!
//! // Generate 256 random bits.
//! let mut csprng = OsRng;
//! let mut raw_key = [0u8; 32];
//! csprng.fill_bytes(&mut raw_key);
//!
//! // Create a secret key from those bits.
//! let secret_key = SecretKey::from_bytes(raw_key);
//! # }
//! ```
//!
//! ## Signing
//!
//! Once generated, the secret key can produce a [Signature] for a messages:
//!
//! ```
//! # fn main() {
//! # use r255b3::SecretKey;
//! # use rand::{RngCore,rngs::OsRng};
//! # let mut csprng = OsRng;
//! # let mut raw_key = [0u8; 32];
//! # csprng.fill_bytes(&mut raw_key);
//! # let secret_key = SecretKey::from_bytes(raw_key);
//! use r255b3::{Domain,Signature};
//!
//! let msg: &[u8] = b"Did gyre and gimble in the wabe:";
//! let domain = "test domain for documentation...";
//! let sig: Signature = secret_key.sign(domain, msg);
//! # }
//! ```
//!
//! ## Verifying
//!
//! Then we can derive the [PublicKey], and check the signature.
//!
//! ```
//! # fn main() {
//! # use r255b3::{SecretKey, Signature, Domain};
//! # use rand::{RngCore,rngs::OsRng};
//! # let mut csprng = OsRng;
//! # let mut raw_key = [0u8; 32];
//! # csprng.fill_bytes(&mut raw_key);
//! # let secret_key = SecretKey::from_bytes(raw_key);
//! # let msg: &[u8] = b"Did gyre and gimble in the wabe:";
//! # let domain = "test domain for documentation...";
//! # let sig: Signature = secret_key.sign(domain, msg);
//! use r255b3::PublicKey;
//!
//! let public_key: PublicKey = secret_key.derive_public();
//! assert!(public_key.verify(domain, &sig, msg).is_ok());
//! # }
//! ```
//! ## Single-pass and incremental operation
//!
//! r255b3 has been designed to accept incremental and streaming inputs.
//!
//! ```
//! # fn main() {
//! # use r255b3::{SecretKey, Domain};
//! # use rand::{RngCore,rngs::OsRng};
//! # let mut csprng = OsRng;
//! # let mut raw_key = [0u8; 32];
//! # csprng.fill_bytes(&mut raw_key);
//! # let secret_key = SecretKey::from_bytes(raw_key);
//! # let public_key = secret_key.derive_public();
//! use r255b3::SignerVerifier;
//!
//! let mut sv: SignerVerifier = SignerVerifier::init("test domain for documentation...");
//!
//! sv.feed(b"`Twas brillig, and the slithy toves");
//! assert!(sv.verify(&public_key, &sv.sign(&secret_key)).is_ok());
//!
//! sv.feed(b"Did gyre and gimble in the wabe:");
//! assert_eq!(sv.sign(&secret_key), secret_key.sign("test domain for documentation...",
//!     b"`Twas brillig, and the slithy tovesDid gyre and gimble in the wabe:"));
//! # }
//! ```
//!
//! With the `std` feature enabled, the [SignerVerifier] also implements [std::io::Write],
//! allowing serialization directly into the signature scheme's input.
mod digester;
mod keys;
mod public;
mod scalar;
mod secret;
mod sig;

pub use digester::SignerVerifier;
pub use keys::Keys;
pub use public::PublicKey;
pub use scalar::Scalar;
pub use secret::SecretKey;
pub use sig::Signature;

use scalar::ShortScalar;

/// A signature domain.
///
/// A [Signature][crate::Signature] is only valid under a particular domain.
///
/// The basic idea here is being able to use the same secret key in multiple contexts,
/// without the signature produced in one context being valid in another.
///
/// E.g. if you are signing values to attest the state of some data when you received it,
/// you don't want that to be able to be used as an endorsement of that data.
pub type Domain = &'static str;

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
/// The errors we can encounter while working with r255b3 keys and signatures.
pub enum Error {
    /// The secret key hasn't had its five fixed bits set correctly.
    InvalidSecretKey,
    /// Failed to decompress a compressed edwards point in the public key.
    FailedDecompression,
    /// A scalar in the signature is not in its cononical representation.
    NonCanonicalSignature,
    /// The reconstructed "e" value doesn't match the signature "e" value.
    SignatureMismatch,
}

impl core::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let msg = match self {
            Error::InvalidSecretKey => "Invalid Secret Key",
            Error::FailedDecompression => "Failed to Decompress Public Key",
            Error::NonCanonicalSignature => "Non-Canonical Signature",
            Error::SignatureMismatch => "Signature Verification Mismatch",
        };
        write!(f, "{}", msg)
    }
}

#[cfg(feature = "std")]
impl std::error::Error for Error {}

#[cfg(test)]
const MSG0: &[u8] = b"'Twas brillig and the slithy toves";
#[cfg(test)]
const MSG1: &[u8] = b"All mimsy were the borogoves,";
#[cfg(test)]
const D0: Domain = "test domain for running tests...";
#[cfg(test)]
const D1: Domain = "test domain for documentation...";

#[test]
fn fail_to_verify_with_wrong_key() {
    use rand::{rngs::OsRng, RngCore};
    let mut csprng = OsRng;
    let mut raw_key = [0u8; 32];

    csprng.fill_bytes(&mut raw_key);
    let ska = SecretKey::from_bytes(raw_key);
    let pka = ska.derive_public();
    let sig = ska.sign("", MSG0);

    csprng.fill_bytes(&mut raw_key);
    let skb = SecretKey::from_bytes(raw_key);
    let pkb = skb.derive_public();

    assert!(pka.verify("", &sig, MSG0).is_ok());
    assert_eq!(pkb.verify("", &sig, MSG0), Err(Error::SignatureMismatch));
}

#[test]
fn fail_to_verify_with_wrong_domain() {
    use rand::{rngs::OsRng, RngCore};
    let mut csprng = OsRng;
    let mut raw_key = [0u8; 32];

    csprng.fill_bytes(&mut raw_key);
    let ska = SecretKey::from_bytes(raw_key);
    let pka = ska.derive_public();
    let sig = ska.sign(D0, MSG0);

    assert!(pka.verify(D0, &sig, MSG0).is_ok());
    assert_eq!(pka.verify(D1, &sig, MSG0), Err(Error::SignatureMismatch));
}

#[test]
fn different_signatures_for_different_messages() {
    use rand::{rngs::OsRng, RngCore};
    let mut csprng = OsRng;
    let mut raw_key = [0u8; 32];

    csprng.fill_bytes(&mut raw_key);
    let ska = SecretKey::from_bytes(raw_key);
    let sv = SignerVerifier::init(D0);
    let n0 = sv.clone().feed(MSG0).sign(&ska);
    let n1 = sv.clone().feed(MSG1).sign(&ska);

    assert_ne!(n0, n1);
}

#[test]
fn different_nonces_for_different_messages() {
    use rand::{rngs::OsRng, RngCore};
    let mut csprng = OsRng;
    let mut raw_key = [0u8; 32];

    csprng.fill_bytes(&mut raw_key);
    let ska = SecretKey::from_bytes(raw_key);
    let pka = ska.derive_public();
    let sv = SignerVerifier::init(D0);
    let n0 = sv.clone().feed(MSG0).derive_nonce(&ska, &pka);
    let n1 = sv.clone().feed(MSG1).derive_nonce(&ska, &pka);

    assert!(n0 != n1);
}

#[test]
fn different_nonces_for_different_domains() {
    use rand::{rngs::OsRng, RngCore};
    let mut csprng = OsRng;
    let mut raw_key = [0u8; 32];

    csprng.fill_bytes(&mut raw_key);
    let ska = SecretKey::from_bytes(raw_key);
    let pka = ska.derive_public();
    let n0 = SignerVerifier::init(D0)
        .clone()
        .feed(MSG0)
        .derive_nonce(&ska, &pka);
    let n1 = SignerVerifier::init(D1)
        .clone()
        .feed(MSG0)
        .derive_nonce(&ska, &pka);

    assert!(n0 != n1);
}

#[test]
fn different_nonces_for_different_public_keys() {
    use rand::{rngs::OsRng, RngCore};
    let mut csprng = OsRng;
    let mut raw_key = [0u8; 32];

    csprng.fill_bytes(&mut raw_key);
    let ska = SecretKey::from_bytes(raw_key);
    let pka = ska.derive_public();
    let pkb = pka.scale(ska.0);
    let mut sv = SignerVerifier::init(D0);
    sv.feed(MSG0);
    let n0 = sv.derive_nonce(&ska, &pka);
    let n1 = sv.derive_nonce(&ska, &pkb);

    assert!(n0 != n1);
}

#[test]
fn scaling_preserves_secret_public_correspondence() {
    use rand::{rngs::OsRng, RngCore};
    let mut csprng = OsRng;
    let mut raw_key = [0u8; 32];
    let mut raw_scalar = [0u8; 32];

    csprng.fill_bytes(&mut raw_key);
    csprng.fill_bytes(&mut raw_scalar);
    let scale = Scalar::from_bytes_mod_order(raw_scalar);

    let sk0 = SecretKey::from_bytes(raw_key);
    let pk0 = sk0.derive_public();

    let sk1 = sk0.scale(scale);
    let pk1 = pk0.scale(scale);

    let pk1_real = sk1.derive_public();

    if pk1_real != pk1 {
        print!("{:?} scaled by {:?} yielded {:?}", sk0.0, scale, sk1.0);
        panic!("{:?} != {:?}", pk1_real.compressed, pk1.compressed);
    }
}

#[test]
fn signature_serdes() {
    use rand::{rngs::OsRng, RngCore};
    let mut csprng = OsRng;
    let mut raw_key = [0u8; 32];

    csprng.fill_bytes(&mut raw_key);
    let ska = SecretKey::from_bytes(raw_key);
    let sig = ska.sign(D0, MSG0);

    let raw = sig.to_bytes();
    assert_eq!(sig, Signature::parse_bytes(raw).unwrap());
}

#[test]
fn test_vectors_pass() {
    let ska = SecretKey::parse_bytes(*b"Did gyre and gimble in the wabe\n").unwrap();
    let pka = ska.derive_public();
    assert_eq!(
        format!("{:x}", pka),
        "161bf5685cfbb674ad88dbbb266e15ec5aa93406135ec471704acb79cd246564"
    );

    let domain = "All mimsy were the borogroves,";
    let msg = b"And the mome raths outgrabe";
    let sig = ska.sign(domain, msg);
    assert_eq!(
        format!("{:x}", sig),
        "9028a67bf5ca00771b15ab30535bba69d4991415d864503cd87e589712d0e0186d4c03629416d51e639817ba9009520c",
    );
}

use curve25519_dalek::{RistrettoPoint, Scalar};

use crate::{Domain, Error, PublicKey, SecretKey, ShortScalar, Signature};

/// Schnorr-Ristretto-Blake3 signer-verifier.
#[derive(Clone)]
pub struct SignerVerifier(blake3::Hasher);

impl SignerVerifier {
    /// Initialize a new signer-verifier
    pub fn init(domain: Domain) -> Self {
        let mut sv = SignerVerifier(blake3::Hasher::new_derive_key(
            "Schnorr-Ristretto255-Blake3",
        ));
        sv.feed(domain.as_bytes()).demark();
        sv
    }

    /// Feed some data into the signer-verifier.
    pub fn feed(&mut self, data: &[u8]) -> &mut Self {
        self.0.update(data);
        self
    }

    /// Separate the input before this point from the input after this point unambiguously.
    pub fn demark(&mut self) -> &mut Self {
        self.0 = blake3::Hasher::new_keyed(&self.digest());
        self
    }

    /// Extract the current digest.
    fn digest(&self) -> [u8; 32] {
        self.0.finalize().into()
    }

    /// Generate a Ristretto255 "random" Scalar, deterministicly derived from the current state of the message and the secret key.
    pub(crate) fn derive_nonce(&self, sk: &SecretKey, pk: &PublicKey) -> Scalar {
        // No need to demark here, because everything is constant size.
        let nonce = self
            .clone()
            .feed("Secret Nonce for Schnorr-Ristretto255-Blake3".as_bytes())
            .feed(sk.as_bytes())
            .feed(pk.as_bytes())
            .digest();
        // Note that this introduces a total deviation from the uniform distribution of ~2⁻¹²⁶.
        // This remains constant up to about a 380-bit hash, whereupon it starts increasing.
        // A 512 bit hash (what Ed25519 does) would decrease this deviation to ~2⁻²⁶¹.
        // Both far surpass the recommended <2⁶⁵ bits of deviation.
        Scalar::from_bytes_mod_order(nonce)
    }

    /// Generate a Ristretto255 "random" Scalar, deterministicly derived from the current state of the message and the secret key.
    pub(crate) fn derive_msg_hash(&self, pk: &PublicKey, r: &RistrettoPoint) -> ShortScalar {
        // No need to demark here, because everything is constant size.
        let hsh = self
            .clone()
            .feed("Message Hash for Schnorr-Ristretto255-Blake3".as_bytes())
            .feed(pk.as_bytes())
            .feed(r.compress().as_bytes())
            .digest();
        ShortScalar::from_32(hsh)
    }

    /// Sign the current message, for the matching public key, with the derived nonce.
    ///
    /// This is the default signing method.
    pub fn sign(&self, sk: &SecretKey) -> Signature {
        let pk = sk.derive_public();
        self.sign_for_pubkey_with_nonce(sk, &pk, &self.derive_nonce(sk, &pk))
    }

    /// Sign the current message, for verification with a particular public key, with the derived nonce.
    ///
    /// Use this version if you already have your public key available or are signing for a different public key than your secret key.
    pub fn sign_for_pubkey(&self, sk: &SecretKey, pk: &PublicKey) -> Signature {
        self.sign_for_pubkey_with_nonce(sk, pk, &self.derive_nonce(sk, pk))
    }

    /// Sign the current message, for the matching public key, with the supplied secret nonce.
    ///
    /// If you really want randomized signatures, this is the function to use.
    /// Make sure you have a really solid random number generator making these nonces!
    pub fn sign_with_nonce(&self, sk: &SecretKey, k: &Scalar) -> Signature {
        self.sign_for_pubkey_with_nonce(sk, &sk.derive_public(), k)
    }

    /// Sign the current message, for verification with a particular public key, with the supplied secret nonce.
    ///
    /// Use this version if you already have your public key available or are signing for a different public key than your secret key.
    ///
    /// If you really want randomized signatures, this is the function to use.
    /// Make sure you have a really solid random number generator making these nonces!
    pub fn sign_for_pubkey_with_nonce(
        &self,
        sk: &SecretKey,
        pk: &PublicKey,
        k: &Scalar,
    ) -> Signature {
        let r = RistrettoPoint::mul_base(k);
        let e = self.derive_msg_hash(&pk, &r);
        let s = k - (e * sk.0);
        Signature { e, s }
    }

    /// Verify a signature with a public key on the current message.
    pub fn verify(&self, pk: &PublicKey, sig: &Signature) -> Result<(), Error> {
        let r_v =
            RistrettoPoint::vartime_double_scalar_mul_basepoint(&sig.e.into(), &pk.point, &sig.s);
        let e_v = self.derive_msg_hash(pk, &r_v);
        if e_v != sig.e {
            return Err(Error::SignatureMismatch);
        };
        Ok(())
    }
}

#[cfg(feature = "std")]
impl std::io::Write for SignerVerifier {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        self.feed(buf);
        Ok(buf.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

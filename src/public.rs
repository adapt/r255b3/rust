use curve25519_dalek::{ristretto::CompressedRistretto, RistrettoPoint, Scalar};
use subtle::ConstantTimeEq;

use crate::{Domain, Error, Signature, SignerVerifier};

#[derive(Clone, Eq)]
/// A r255b3 public key to verify signatures.
pub struct PublicKey {
    /// The compressed version of the public key.
    pub(crate) compressed: CompressedRistretto,
    /// The uncompressed version of the public key.
    pub(crate) point: RistrettoPoint,
}

/// This is based only on the the compressed point, and is constant time.
impl PartialEq for PublicKey {
    fn eq(&self, other: &Self) -> bool {
        self.ct_eq(other).into()
    }
}

impl core::fmt::LowerHex for PublicKey {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        for x in self.compressed.to_bytes() {
            write!(f, "{:02x}", x)?;
        }
        Ok(())
    }
}

impl core::fmt::Debug for PublicKey {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "PublicKey {:x}", *self)
    }
}

impl ConstantTimeEq for PublicKey {
    fn ct_eq(&self, other: &Self) -> subtle::Choice {
        self.compressed.ct_eq(&other.compressed)
    }
}

impl PublicKey {
    /// Parse a public key from a byte array.
    ///
    /// This verifies that is it properly formatted in canonical form.
    pub fn parse_bytes(value: [u8; 32]) -> Result<PublicKey, Error> {
        let compressed = CompressedRistretto(value);
        let point = compressed.decompress().ok_or(Error::FailedDecompression)?;
        Ok(PublicKey { compressed, point })
    }

    /// The underlying byte fromat for serialization.
    pub fn as_bytes(&self) -> &[u8; 32] {
        self.compressed.as_bytes()
    }

    /// Verify a signature on a message, in a particular domain, using this public key.
    pub fn verify(&self, domain: Domain, sig: &Signature, msg: &[u8]) -> Result<(), Error> {
        SignerVerifier::init(domain).feed(msg).verify(self, sig)
    }

    /// Scale by a public scalar to derive a subkey.
    ///
    /// Scaling the corresponding secret key by the same scalar will produce a valid key pair.
    pub fn scale(&self, by: Scalar) -> Self {
        let ep = self.point * by;
        PublicKey {
            compressed: ep.compress(),
            point: ep,
        }
    }
}
